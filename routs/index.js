import authRouter from "./authRouter.js";
import userRouter from './userRouter.js'
import categoryRouter from "./categoryRouter.js";
import productRouter from "./productRouter.js";
import cartRouter from "./cartRouter.js";
import orderRouter from './orderRouter.js'

export {
    authRouter,
    userRouter,
    categoryRouter,
    productRouter,
    cartRouter,
    orderRouter
}
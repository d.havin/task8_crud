import {
  REQUEST_CREATE_USER,
  REQUEST_CREATE_USER_SUCCEEDED,
  REQUEST_CREATE_USER_FAILED,
  FETCH_CREATE_USER,

  EDIT_USER,
  DELETE_USER,

  REQUESTED_USER,
  REQUESTED_USER_SUCCEEDED,
  REQUESTED_USER_FAILED,
  FETCHED_USER,
} from "../constants/user.constants";

export const requestCreateUser = () => ({ type: REQUEST_CREATE_USER});
export const requestCreateUserSuccess = (data: any) => ({ type: REQUEST_CREATE_USER_SUCCEEDED, data }); // <= ???????
export const requestCreateUserFailed = () => ({ type: REQUEST_CREATE_USER_FAILED });
export const fetchCreateUser = (data: any) => ({ type: FETCH_CREATE_USER, data });       // <=== ??

export const editUser = (data: any) => ({ type: EDIT_USER, data });
export const deleteUser = (data: any) => ({ type: DELETE_USER, data });

export const requestUser = () => {return { type: REQUESTED_USER }};
export const requestUserSuccess = (data: any) => {return { type: REQUESTED_USER_SUCCEEDED, mongoData: data.users}};
export const requestUserError = () => {return { type: REQUESTED_USER_FAILED }};
export const fetchUser = () => {return { type: FETCHED_USER }};

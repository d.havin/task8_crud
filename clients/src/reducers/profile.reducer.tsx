import {
  REQUEST_CREATE_USER,
  REQUEST_CREATE_USER_SUCCEEDED,
  REQUEST_CREATE_USER_FAILED,

  EDIT_USER,
  DELETE_USER,

  REQUESTED_USER,
  REQUESTED_USER_SUCCEEDED,
  REQUESTED_USER_FAILED,
} from "../constants/user.constants";

const initialState = {
  mongoData: [],
  loading: false,
  error: false,
};

const profileReducer = (state = initialState, action: any) => {
  switch (action.type) {

    case REQUEST_CREATE_USER:
      return {
        mongoData: [],
        loading: true,
        error: false,
      };
    case REQUEST_CREATE_USER_SUCCEEDED:
      return {
        mongoData: action.mongoData,   //     <== ??
        loading: false,
        error: false,
      };
    case REQUEST_CREATE_USER_FAILED:
      return {
        mongoData: [],
        loading: false,
        error: true,
      };




    case EDIT_USER:
      return;
    case DELETE_USER:
      return;


    case REQUESTED_USER:
      return {
        mongoData: [],
        loading: true,
        error: false,
      };
    case REQUESTED_USER_SUCCEEDED:
      return {
        mongoData: action.mongoData,
        loading: false,
        error: false,
      };
    case REQUESTED_USER_FAILED:
      return {
        mongoData: [],
        loading: false,
        error: true,
      };
    default:
      return state;
  }
};

export default profileReducer;

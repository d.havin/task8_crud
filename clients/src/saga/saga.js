import {
  requestCreateUser,
  requestCreateUserSuccess,
  requestCreateUserFailed,

  requestUser,
  requestUserSuccess,
  requestUserError,
} from "../actions/user.action";
import { put, call, takeEvery } from "redux-saga/effects";

//====================================================================getAllUsers
// генератор - наблюдатель
export function* watchFetchUser() {
  yield takeEvery("FETCHED_USER", fetchUserAsync);
  yield takeEvery("FETCH_CREATE_USER", fetchCreateUserAsync)
}

// worker-saga
function* fetchUserAsync() {
  try {
    yield put(requestUser());
    const data = yield call(() => {
      return fetch("http://localhost:3030/user/allUsers", {
        method: "GET",
        headers: {
          "Content-type": "application/json; charset = UTF-8",
        },
      }).then((res) => res.json());
    },
    );
    yield put(requestUserSuccess(data));
  } catch (error) {
    yield put(requestUserError());
  }
} 

//============================================================CREATE User

// worker-saga
function* fetchCreateUserAsync(action) {
  try {
    console.log(action)
    yield put(requestCreateUser());
    const data = yield call((data) => {
      return fetch("http://localhost:3030/user/create", {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
          "Content-type": "application/json; charset = UTF-8",
        },
      })
    });
    console.log(data)
    yield put(requestCreateUserSuccess(data));
  } catch (error) {
    yield put(requestCreateUserFailed());
  }
} 


// export async function createUser(data) {
//   await fetch(`http://localhost:3030/user/create`, {                                    <=== для обычного фетча
//       method: 'POST',
//       body: JSON.stringify(data),
//       headers: {
//           'Content-type': 'application/json; charset=UTF-8',
//       },
//   })  
// };

//======================================================DELETE User






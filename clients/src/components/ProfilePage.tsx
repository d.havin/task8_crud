import React, { useState } from 'react';
// import '../styles/profilePage.css';
import { fetchCreateUser, editUser, deleteUser, fetchUser, } from "../actions/user.action";
import { connect } from "react-redux";
import ProfileInput from './ProfileInput.component';
// import {createUser} from '../saga/saga'  <=== для обычного фетча

const ProfileInfo = ( props: any ) => {
    const [editedName, setName] = useState('');
    const [editedSurName, setSurName] = useState('');
    const [editedEmail, setEmail] = useState('');
    const [editedPassword, setPassword] = useState('');
    // const [data, setData] = useState('')

    const handleCreate = () => {
        let user = {
            name: editedName,
            surname: editedSurName,
            email: editedEmail, 
            password: editedPassword
        }
        fetchCreateUser(user);
        // createUser(user) // <=== для обычного фетча
        console.log(user)

        setName('');
        setSurName('');
        setEmail('');
        setPassword('');
    }

    const handleEdit = () => {
        let user = {
            name: editedName,
            surName: editedSurName,
            email: editedEmail,
            password: editedPassword
        }
        props.editUser(user);

        setName('');
        setSurName('');
        setEmail('');
        setPassword('');
    }

    const handleDelete = () => {
        props.deleteUser()
    }

    const handleList = () => {
        props.fetchUser()
    }

    return (
        <div className="editInfoMenu">
            Имя: <ProfileInput data={editedName} setInputData={setName} />
            Фамилия: <ProfileInput data={editedSurName} setInputData={setSurName} />
            Email: <ProfileInput data={editedEmail} setInputData={setEmail}/>
            Пароль: <ProfileInput data={editedPassword} setInputData={setPassword} />
            <button className="profButton" onClick={() => handleCreate()}>Создать</button>
            <button className="profButton" onClick={() => handleList()}>Список</button>
            {props.profile.loading     
                ? <p>Loading...</p>
                : props.profile.error
                    ? <p>Error, try again</p>
                    : props.profile.mongoData.map((user: any) => ( 
                        <ul>
                            <li>
                                {user.name}
                                <button className="profButton" onClick={() => handleEdit()}>Изменить</button>
                                <button className="profButton" onClick={() => handleDelete()}>Удалить</button>
                            </li>
                        </ul>
                    ))
            }
        </div>
    )
}

const mapStateToProps = (state: any) => ({
    profile: state.profile
});

const mapDispatchToProps = (dispatch: any) => ({
    fetchCreateUser: (data: any) => dispatch(fetchCreateUser(data)),
    // createUser: (data: any) => dispatch(createUser(data)),   <=== для обычного фетча
    // editUser: (data:any) => dispatch(editUser(data)),
    // deleteUser: (data) => dispatch(deleteUser(data)),
    fetchUser: () => dispatch(fetchUser())
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ProfileInfo);


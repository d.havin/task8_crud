import React from "react";

function ProfileInput(props: any) {
  return (
    <div className="profileInput">
      <input
        value={props.data}
        type="text"
        onChange={(e) => props.setInputData(e.target.value)}
      />
      {/* {props.flag && <HandleError length = {props.data.length}/>} */}
    </div>
  );
}

export default ProfileInput;

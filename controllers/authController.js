import { User } from "../models/user.js";
import jwt from 'jsonwebtoken';
import { validationResult } from "express-validator";
import { google } from 'googleapis';
import urlParse from 'url-parse';
import queryParse from 'query-string';
import axios from "axios";

const accessTokenSecret = 'youraccesstokensecret';

const clientID = '290625159795-pmjqej23kka2r1of4s7ao9e8tgvuf2vk.apps.googleusercontent.com';
const clientSecret = 'z6CEeni1pgRgCzpegH_2InYB';
const redirect = 'http://localhost:3000/auth/google/gettoken';

const oauth2Client = new google.auth.OAuth2(
    clientID,
    clientSecret,
    redirect);

const registration = async function (request, response) {
    try {
        const errors = validationResult(request)
        if (!errors.isEmpty()) {
            let err = errors;
            return response.status(400).send(` error message: ${err.errors[0].msg}`)
        }
        let user = await User.findOne({ email: request.body.email });
        if (user) {
            return response.status(400).send(`This email is already in use`)
        }
        const newUser = await new User({ name: request.body.name, surname: request.body.surname, email: request.body.email, password: request.body.password, role: request.body.role });
        await newUser.save();
        response.send(
            `Registration successfully. User name: ${newUser.name} ${newUser.surname}`);
    } catch (err) {
        response.status(500).send(`Something went wrong`)
    }

}

const authorization = async function (request, response) {
    const user = await User.findOne({ email: request.body.email, password: request.body.password });
    if (!user) {
        return response.status(400).send('Wrong password or email');
    }
    const accessToken = jwt.sign({ name: user.name, role: user.role, _id: user._id }, accessTokenSecret, { expiresIn: '56565656m' });
    response.json({
        accessToken
    });
}

const authenticateJWT = (request, response, next) => {
    const authHeader = request.headers.authorization;
    if (authHeader) {
        const token = authHeader.split(' ')[1];
        jwt.verify(token, accessTokenSecret, (err, user) => {
            if (err) {
                return response.sendStatus(403);
            }
            request.user = user;
            next();
        });
    } else {
        response.sendStatus(401);
    }
}

const checkAccess = function (request, response, next) {
    if (request.user.role !== 'admin') {
        return response.status(403).json({ message: 'You do not have access' });
    }
    next()
};

const createGoogleUrl = async function (request, response) {
    try {
        const url = oauth2Client.generateAuthUrl({
            access_type: 'offline',
            scope: ['https://www.googleapis.com/auth/userinfo.profile',
                'https://www.googleapis.com/auth/userinfo.email'
            ]
        });
        response.send({ url });
    } catch (err) {
        response.status(403).send(`Can't create url`)
    }
}

const getToken = async function (request, response) {
    try {
        const queryURL = new urlParse(request.url);
        const code = queryParse.parse(queryURL.query).code;
        const { tokens } = await oauth2Client.getToken(code);

        const { data } = await axios({
            url: 'https://www.googleapis.com/oauth2/v2/userinfo',
            method: 'get',
            headers: {
                authorization: `Bearer ${tokens.access_token}`
            }
        })

        let user = await User.findOne({ email: data.email });
        if (!user) {
            const newUser = new User({ name: data.given_name, surname: data.family_name, email: data.email });
            await newUser.save();
            const accessToken = jwt.sign({ name: newUser.name, role: newUser.role, _id: newUser._id }, accessTokenSecret, { expiresIn: '56565656m' });
            response.send(`Create new user: ${newUser.name} ${newUser.surname},
                        accessToken: ${accessToken}`)
        }
        else {
            response.status(403).send(`Can't get token`)
        }
    } catch (err) {
        response.status(500).send(`Your google account was already used for authorization`)
    }
}

export {
    registration,
    authorization,
    authenticateJWT,
    checkAccess,
    createGoogleUrl,
    getToken
}
import { validationResult } from "express-validator";
import { User } from "../models/user.js";

const createUser = async function (request, response) {
  // console.log("1")
  // try {
  //   const errors = validationResult(request);
  //   if (!errors.isEmpty()) {
  //     let err = errors;
  //     return response.status(400).send(` error message: ${err.errors[0].msg}`);
  //   }


  //   let user = await User.findOne({ email: request.body.email });
  //   if (user) {
  //     return response.status(400).send(`Such user is already existed`);
  //   }

    console.log('9')
    const newUser = new User({ name: request.body.name, surname: request.body.surname, email: request.body.email, password: request.body.password });
    console.log('10')
    await newUser.save();
    response.json(`Create new user:
                        user name: ${newUser.name}
                        user surname: ${newUser.surname}
                        email: ${newUser.email}`);
    console.log({newUser})
    console.log('11')
  // } catch (err) {
  //   response.status(500).json(`Something went wrong`);
  // }
};

const deleteUser = async function (request, response) {
  try {
    let user = await User.findOne({ _id: request.params.id });
    if (!user) {
      return response.send(`Can't find this user.`);
    }
    User.deleteOne({ _id: request.params.id }, function (err, result) {
      if (err) return response.status(400).send(`Can't find this user.`);
    });
    response.send(`Delete user ${user.name} ${user.surname}`);
  } catch (err) {
    response.status(500).send(`Something went wrong. Can't delete this user.`);
  }
};

const updateUser = async function (request, response) {
  try {
    let user = await User.findOne({ _id: request.params.id });
    // if (!user) {
    //   return response.status(400).send(`Can't find this user`);
    // }
    // if (request.user.role == "admin" || request.user._id == request.params.id) {
      User.updateOne(
        { _id: request.params.id },
        { surname: request.body.surname },
        { new: true },
        function (err, result) {
          if (err) return response.status(400).send(`Can't find this user.`);
        }
      );
      return response.send(`Update user ${user.name} ${user.surname}`);
    // }
    // response.send(`It's not your profile. You don't have access yo update it`);
  } catch (err) {
    response.status(500).send(`Something went wrong. Can't update this user.`);
  }
};

const getUserById = async function (request, response) {
  try {
    let user = await User.findOne({ _id: request.params.id });
    if (!user) {
      return response.send(`Can't find such user`);
    }
    response.send(`user name: ${user.name}
                        user surname: ${user.surname}
                        email: ${user.email}`);
  } catch (err) {
    response
      .status(500)
      .send(`Something went wrong. Can't see the user by id.`);
  }
};

const getAllUsers = async function (request, response) {
  try {
    let users = await User.find({ role: "user" });
    if (!users) {
      return response.status(400).json(`Can't find such users`);
    }
    response.json({users});
  } catch (err) {
    response.status(500).json(`Something went wrong. Can't see all users.`);
  }
};

export { createUser, deleteUser, updateUser, getAllUsers, getUserById };
